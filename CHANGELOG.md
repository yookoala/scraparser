## v0.3.1
- Bugfix
- Add `fix date-column-overerflow` subcommand.

## v0.3.0

* Internal rewrite and prepare for v0.3.0
* BREAKING: `fix-*` subscommand are now `fix *`.

## v0.2.4
Subcommand `parse_pdf_to_csv` add options:
 
 * --pages:
   exposing camelot.parse_pdf() pages parameter to specify the pages
   in the PDF to parse with.
    
 * --offset:
   to specify the number of tables to skip before start parsing.
    
 * --limit:
   to specify the number of tables in total to parse.
