SHELL:=/bin/bash
TIMESTAMP:=$(shell date +%Y%m%d%H%M%S)
TAG_VERSION:=$(shell git describe --tags --abbrev=0 --match="v*.*.*" | sed -E 's/^v//')
TAG_POST_COUNT:=$(shell git rev-list $(shell git describe --tags --abbrev=0)..HEAD --count)
PYPI_PKG_NAME?=scraparser
ifeq (${TAG_POST_COUNT}, 0)
PYPI_PKG_VERSION?=$(TAG_VERSION)
else
PYPI_PKG_VERSION?=$(TAG_VERSION).post$(TAG_POST_COUNT).dev$(TIMESTAMP)
endif
VENV_NAME?=.venv
PYPI_USERNAME?=__token__
PYPI_PASSWORD?=password
PYPI_TEST_USERNAME?=__token__
PYPI_TEST_PASSWORD?=password
PYTHON:=${VENV_NAME}/bin/python3

.venv:
	python3 -m venv ${VENV_NAME}

all: dist

venv: .venv
	. .venv/bin/activate
	${PYTHON} -m pip install --upgrade pip

version:
	@echo ${PYPI_PKG_VERSION}

dist: venv
	${PYTHON} -m pip install --upgrade setuptools wheel
	PACKAGE_NAME=${PYPI_PKG_NAME} VERSION=${PYPI_PKG_VERSION} ${PYTHON} setup.py sdist bdist_wheel

upload: venv
	${PYTHON} -m pip install --upgrade twine
	${PYTHON} -m twine upload -u "${PYPI_USERNAME}" -p "${PYPI_PASSWORD}" dist/*

upload-test: venv
	${PYTHON} -m pip install --upgrade twine
	${PYTHON} -m twine upload -u "${PYPI_TEST_USERNAME}" -p "${PYPI_TEST_PASSWORD}" --repository testpypi dist/*

clean:
	rm -Rf dist build *.egg-info

.PHONY: all venv version dist clean upload-test upload
