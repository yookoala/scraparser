import setuptools
import os, re, subprocess, sys

with open("README.md", "r") as fh:
    long_description = fh.read()

# Rewrite all local paths in the markdown
# to the commit specific gitlab path.
commit_hash = subprocess.check_output(["git", "rev-parse", "--verify", "HEAD"]).decode("utf-8").strip("\r\n")
project_path = "yookoala/scraparser";
commit_url_base = "https://gitlab.com/{project_path}/-/blob/{commit_hash}/".format(project_path=project_path, commit_hash=commit_hash)
long_description = re.sub(
    r'\[(.+?)\]\((?!(http|https):\/\/)(.+?)\)',
    r'[\1](' + commit_url_base + r'\3)',
    long_description
)

# Determine package name and version
# from environment overrides, if any
name = os.getenv("PACKAGE_NAME")
if name is None:
    name = "scraparser"

version = os.getenv("VERSION")
if version is None:
    version = "0.1.0"

# Run setup
setuptools.setup(
    name=name,
    version=version,
    scripts=[],
    author="Koala Yeung",
    author_email="koalay@gmail.com",
    description="A simplified PDF table scraping and parsing tool",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/yookoala/scraparser",
    project_urls={
        "Code": "https://gitlab.com/yookoala/scraparser",
        "Issue tracker": "https://gitlab.com/yookoala/scraparser/-/issues",
    },
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Environment :: Console",
        "Topic :: Office/Business :: Financial :: Spreadsheet",
        "Topic :: Utilities",
    ],
    install_requires=[
        "camelot-py>=0.7"
        "click>=7.1"
        "google-api-python-client>=1.8"
        "google-auth-httplib2>=0.0.3"
        "google-auth-oauthlib>=0.4"
        "opencv-python>=4.2"
        "python-magic>=0.4"
        "validators>=0.14"
    ],
    python_requires=">=3.6",
 )
